package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text1 = findViewById(R.id.textView1);
        but0 = findViewById(R.id.button0);
        but1 = findViewById(R.id.button1);
        but2 = findViewById(R.id.button2);
        but3 = findViewById(R.id.button3);
        but5 = findViewById(R.id.button5);
        but6 = findViewById(R.id.button6);
        but7 = findViewById(R.id.button7);
        but9 = findViewById(R.id.button9);
        but10 = findViewById(R.id.button10);
        but11 = findViewById(R.id.button11);

        form.setRoundingMode(RoundingMode.HALF_UP);
    }

    TextView text1;
    Button but0;
    Button but1;
    Button but2;
    Button but3;
    Button but5;
    Button but6;
    Button but7;
    Button but9;
    Button but10;
    Button but11;
    Double i;
    Character action;
    Double result;
    DecimalFormat form = new DecimalFormat("#.##");

    public void but1Click(View view) {
        if (text1.getText().equals("0")) {
            text1.setText(but1.getText());
        } else {
            text1.setText(text1.getText().toString() + but1.getText().toString());
        }
        if (result != null) {
            result = null;
            text1.setText(but1.getText());
        }

    }

    public void but2Click(View view) {
        if (text1.getText().equals("0")) {
            text1.setText(but2.getText());
        } else {
            text1.setText(text1.getText().toString() + but2.getText().toString());
        }
        if (result != null) {
            result = null;
            text1.setText(but2.getText());
        }
    }

    public void but3Click(View view) {
        if (text1.getText().equals("0")) {
            text1.setText(but3.getText());
        } else {
            text1.setText(text1.getText().toString() + but3.getText().toString());
        }
        if (result != null) {
            result = null;
            text1.setText(but3.getText());
        }
    }

    public void buttonResultClick(View view) {
        calculate();
        i = 0.0;
    }

    public void but4Click(View view) {
        if (text1.getText().equals("0")) {
            text1.setText(but5.getText());
        } else {
            text1.setText(text1.getText().toString() + but5.getText().toString());
        }
        if (result != null) {
            result = null;
            text1.setText(but5.getText());
        }
    }

    public void but5Click(View view) {
        if (text1.getText().equals("0")) {
            text1.setText(but6.getText());
        } else {
            text1.setText(text1.getText().toString() + but6.getText().toString());
        }
        if (result != null) {
            result = null;
            text1.setText(but6.getText());
        }
    }

    public void but6Click(View view) {
        if (text1.getText().equals("0")) {
            text1.setText(but7.getText());
        } else {
            text1.setText(text1.getText().toString() + but7.getText().toString());
        }
        if (result != null) {
            result = null;
            text1.setText(but7.getText());
        }
    }

    public void buttonPlusClick(View view) {
        calculate();
        i = Double.valueOf(text1.getText().toString());
        text1.setText("0");
        action = '+';
    }

    public void but7Click(View view) {
        if (text1.getText().equals("0")) {
            text1.setText(but9.getText());
        } else {
            text1.setText(text1.getText().toString() + but9.getText().toString());
        }
        if (result != null) {
            result = null;
            text1.setText(but9.getText());
        }
    }

    public void but8Click(View view) {
        if (text1.getText().equals("0")) {
            text1.setText(but10.getText());
        } else {
            text1.setText(text1.getText().toString() + but10.getText().toString());
        }
        if (result != null) {
            result = null;
            text1.setText(but10.getText());
        }
    }

    public void but9Click(View view) {
        if (text1.getText().equals("0")) {
            text1.setText(but11.getText());
        } else {
            text1.setText(text1.getText().toString() + but11.getText().toString());
        }
        if (result != null) {
            result = null;
            text1.setText(but11.getText());
        }
    }

    public void buttonMinusClick(View view) {
        calculate();
        i = Double.valueOf(text1.getText().toString());
        text1.setText("0");
        action = '-';
    }

    public void but0Click(View view) {
        if (text1.getText().equals("0")) {
            text1.setText(but0.getText());
        } else {
            text1.setText(text1.getText().toString() + but0.getText().toString());
        }
        if (result != null) {
            result = null;
            text1.setText(but0.getText());
        }
    }

    public void buttonMultiClick(View view) {
        calculate();
        i = Double.valueOf(text1.getText().toString());
        text1.setText("0");
        action = '*';
    }

    public void butClearClick(View view) {
        clear();
    }

    public void butInvClick(View view) {
        Double num = Double.valueOf(text1.getText().toString());
        num *= -1;
        text1.setText(form.format(num));
    }

    public void butPercentClick(View view) {
        i = Double.valueOf(text1.getText().toString());
        i /= 100.0;
        text1.setText(form.format(i));
    }

    public void buttonDivClick(View view) {
        calculate();
        i = Double.valueOf(text1.getText().toString());
        text1.setText("0");
        action = '/';
    }

    private void clear() {
        result = null;
        i = null;
        action = null;
        text1.setText("0");
    }

    private void calculate() {
        if (i != null && action != null) {
            switch (action) {
                case '+':
                    result = i + Double.valueOf(text1.getText().toString());
                    text1.setText(form.format(result));
                    break;
                case '-':
                    result = i - Double.valueOf(text1.getText().toString());
                    text1.setText(form.format(result));
                    break;
                case '*':
                    result = i * Double.valueOf(text1.getText().toString());
                    text1.setText(form.format(result));
                    break;
                case '/':
                    try {
                        result = i / Double.valueOf(text1.getText().toString());
                        text1.setText(form.format(result));
                    } catch (Exception e) {
                        Toast.makeText(this, "Делить на ноль нельзя!", Toast.LENGTH_SHORT).show();
                        clear();
                    }
                    break;
            }
        }
    }

    public void butSin(View view) {
        i = Double.valueOf(text1.getText().toString());
        result = Math.sin(Math.toRadians(i));
        text1.setText(form.format(result));
    }

    public void butCos(View view) {
        i = Double.valueOf(text1.getText().toString());
        result = Math.cos(Math.toRadians(i));
        text1.setText(form.format(result));
    }

    public void butTan(View view) {
        i = Double.valueOf(text1.getText().toString());
        result = Math.tan(Math.toRadians(i));
        text1.setText(form.format(result));
    }

    public void butFact(View view) {
        result = 1.0;
        i = Double.valueOf(text1.getText().toString());
        action = '!';
        for(int j = 1; j <= i; j++){
            result *= j;
        }
        text1.setText(form.format(result));
    }

    public void butSqr(View view) {
        i = Double.valueOf(text1.getText().toString());
        result = i * i;
        action = '^';
        text1.setText(form.format(result));
    }

    public void buttonPoint(View view) {
        if(!text1.getText().toString().contains(".")){
            text1.setText(text1.getText().toString() + ".");
        }
    }
}
